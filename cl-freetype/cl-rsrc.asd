;;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; Base: 10 -*-

(in-package :asdf)

(defsystem cl-rsrc
    :name "cl-rsrc"
  :author "Yusuke Shinyama <yusuke at cs dot nyu dot edu>"
  :version "0.1"
  :serial t
  :components ((:file "cl-rsrc")))
    
