;; -*- lisp-version: "8.0 [Windows] (May 22, 2006 0:51)"; cg: "1.81"; -*-

(in-package :cg-user)

(defpackage :CELLO)

(define-project :name :cello-demo
  :modules (list (make-instance 'module :name "cellodemo.lisp")
                 (make-instance 'module :name "demo-window.lisp")
                 (make-instance 'module :name "tutor-geometry.lisp")
                 (make-instance 'module :name "light-panel.lisp")
                 (make-instance 'module :name "hedron-render.lisp")
                 (make-instance 'module :name
                                "hedron-decoration.lisp"))
  :projects (list (make-instance 'project-module :name "..\\cello"))
  :libraries nil
  :distributed-files nil
  :internally-loaded-files nil
  :project-package-name :cello
  :main-form nil
  :compilation-unit t
  :verbose nil
  :runtime-modules '(:cg-dde-utils :cg.base)
  :splash-file-module (make-instance 'build-module :name "")
  :icon-file-module (make-instance 'build-module :name "")
  :include-flags '(:local-name-info)
  :build-flags '(:allow-debug :purify)
  :autoload-warning t
  :full-recompile-for-runtime-conditionalizations nil
  :default-command-line-arguments "+cx +t \"Initializing\""
  :additional-build-lisp-image-arguments '(:read-init-files nil)
  :old-space-size 256000
  :new-space-size 6144
  :runtime-build-option :standard
  :on-initialization 'cello::cello-test
  :on-restart 'do-default-restart)

;; End of Project Definition
