;; -*- lisp-version: "8.1 [Windows] (Mar 4, 2008 15:30)"; cg: "1.103.2.10"; -*-

(in-package :cg-user)

(defpackage :CELLO)

(define-project :name :cello
  :modules (list (make-instance 'module :name "cello.lisp")
                 (make-instance 'module :name "window-macros.lisp")
                 (make-instance 'module :name "clipping.lisp")
                 (make-instance 'module :name "colors.lisp")
                 (make-instance 'module :name "ix-layer-expand.lisp")
                 (make-instance 'module :name "frame.lisp")
                 (make-instance 'module :name "application.lisp")
                 (make-instance 'module :name "image.lisp")
                 (make-instance 'module :name "ix-togl.lisp")
                 (make-instance 'module :name "ix-opengl.lisp")
                 (make-instance 'module :name "ix-canvas.lisp")
                 (make-instance 'module :name "font.lisp")
                 (make-instance 'module :name "ix-grid.lisp")
                 (make-instance 'module :name "mouse-click.lisp")
                 (make-instance 'module :name "control.lisp")
                 (make-instance 'module :name "focus.lisp")
                 (make-instance 'module :name "focus-navigation.lisp")
                 (make-instance 'module :name "focus-utilities.lisp")
                 (make-instance 'module :name "ix-styled.lisp")
                 (make-instance 'module :name "ix-text.lisp")
                 (make-instance 'module :name "lighting.lisp")
                 (make-instance 'module :name "ctl-toggle.lisp")
                 (make-instance 'module :name "ctl-markbox.lisp")
                 (make-instance 'module :name "ctl-drag.lisp")
                 (make-instance 'module :name "ctl-selectable.lisp")
                 (make-instance 'module :name "slider.lisp")
                 (make-instance 'module :name "cello-window.lisp")
                 (make-instance 'module :name "window-utilities.lisp")
                 (make-instance 'module :name "wm-mouse.lisp")
                 (make-instance 'module :name "pick.lisp")
                 (make-instance 'module :name "ix-paint.lisp")
                 (make-instance 'module :name "ix-polygon.lisp")
                 (make-instance 'module :name "cello-ftgl.lisp")
                 (make-instance 'module :name "cello-magick.lisp")
                 (make-instance 'module :name "cello-openal.lisp")
                 (make-instance 'module :name "nehe-06.lisp")
                 (make-instance 'module :name "rms.lisp"))
  :projects (list (make-instance 'project-module :name
                                 "..\\Cells\\cells")
                  (make-instance 'project-module :name
                                 "cffi-extender\\cffi-extender")
                  (make-instance 'project-module :name
                                 "kt-opengl\\kt-opengl")
                  (make-instance 'project-module :name
                                 "cl-freetype\\cl-freetype")
                  (make-instance 'project-module :name
                                 "cl-ftgl\\cl-ftgl")
                  (make-instance 'project-module :name
                                 "cl-openal\\cl-openal")
                  (make-instance 'project-module :name
                                 "..\\Cells\\gui-geometry\\gui-geometry")
                  (make-instance 'project-module :name
                                 "cl-magick\\cl-magick")
                  (make-instance 'project-module :name
                                 "..\\Celtk\\CELLOTK")
                  (make-instance 'project-module :name
                                 "C:\\1-devtools\\cl-s3\\cl-s3"))
  :libraries nil
  :distributed-files nil
  :internally-loaded-files nil
  :project-package-name :cello
  :main-form nil
  :compilation-unit t
  :verbose nil
  :runtime-modules nil
  :splash-file-module (make-instance 'build-module :name "")
  :icon-file-module (make-instance 'build-module :name "")
  :include-flags (list :local-name-info)
  :build-flags (list :allow-debug :purify)
  :autoload-warning t
  :full-recompile-for-runtime-conditionalizations nil
  :include-manifest-file-for-visual-styles t
  :default-command-line-arguments "+cx +t \"Initializing\""
  :additional-build-lisp-image-arguments (list :read-init-files nil)
  :old-space-size 256000
  :new-space-size 6144
  :runtime-build-option :standard
  :on-initialization 'cello::nehe-06
  :on-restart 'do-default-restart)

;; End of Project Definition
